const mysqlPW = require('../../KEYS/mysqlPW.js');
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
// support parsing of application/json type post data
app.use(bodyParser.json());
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

const port = 5000;

const db = mysql.createConnection({
  host: 'localhost',
  user: 'cameron',
  password: mysqlPW,
  database: 'flashcards'
})

db.connect();

app.get('/cards', (req, res) => {
  // const sql = 'SELECT * FROM cards';
  const sql = 'SELECT * FROM cards INNER JOIN categories ON cards.category=categories.category_id;'

  db.query(sql, (err, result) => {
    if (err) throw err;
    res.send(result);
  })
});

app.get('/cards/categories', (req, res) => {
  const sql = 'SELECT * FROM categories;';

  db.query(sql, (err, result) => {
    if (err) throw err;
    res.send(result);
  })
})

app.post('/cards/addCard', (req, res) => {
  const question = req.body.question;
  const answer = req.body.answer;
  const category = req.body.category;

  const sql = `INSERT INTO cards (question, answer, category) VALUES ("${question}", "${answer}", "${category}");`;

  db.query(sql, (err, result) => {
    if (err) throw err;
    res.send(result);
  })
})

app.post('/categories/addCategory', (req, res) => {
  const category = req.body.category;

  const sql = `INSERT INTO categories (category) VALUES ("${category}");`;

  db.query(sql, (err, result) => {
    if (err) throw err;
    res.send(result);
  })
})



app.listen(port, () => {console.log(`server started on port ${port}`)})
