var testData = [
  {
    id: 1,
    front: 'front of card 1',
    rear: 'rear of card 1',
  },
  {
    id: 2,
    front: 'front of card 2',
    rear: 'rear of card 2',
  },
  {
    id: 3,
    front: 'front of card 3',
    rear: 'rear of card 3',
  },
  {
    id: 4,
    front: 'front of card 4',
    rear: 'rear of card 4',
  },
  {
    id: 5,
    front: 'front of card 5',
    rear: 'rear of card 5',
  },
  {
    id: 6,
    front: 'front of card 6',
    rear: 'rear of card 6',
  },
  {
    id: 7,
    front: 'front of card 7',
    rear: 'rear of card 7',
  }
]

export default testData;
