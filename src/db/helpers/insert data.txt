CREATE TABLE cards(
card_number INT AUTO_INCREMENT,
question VARCHAR(255),
answer TEXT,
category VARCHAR(255),
PRIMARY KEY(card_number)
);