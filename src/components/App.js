import './styles/App.css';
import Card from './Card';
import Header from './Header';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const App = () => {
  const [cards, setCards] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);

  useEffect(() => {
    axios.get('http://127.0.0.1:5000/cards')
    .then((response) => {
      setCards(response.data)
    })
    .catch((error) => {
      console.log(error);
    });

    axios.get('http://127.0.0.1:5000/cards/categories')
    .then((response) => {
      setCategories(response.data);
      setSelectedCategory(response.data[0].category)
    })
    .catch((error) => {
      console.log(error);
    })
  }, [])

  const handleSelectCategory = (e) => {
    console.log(e.target.value)
    setSelectedCategory(e.target.value);
  }

  const handleAddedCard = () => {
    axios.get('http://127.0.0.1:5000/cards')
    .then((response) => {
      setCards(response.data)
    })
    .catch((error) => {
      console.log(error);
    })
  }

  const handleNewCategoryCreation = (newCategory) => {
    axios.get('http://127.0.0.1:5000/cards/categories')
    .then((response) => {
      setCategories(response.data);
      setSelectedCategory(response.data[response.data.length - 1].category)
    })
    .catch((error) => {
      console.log(error);
    })
  }

  return (
    <div className="App">
      <Header
        categories={categories}
        handleSelectCategory={handleSelectCategory}
        selectedCategory={selectedCategory}
        handleAddedCard={handleAddedCard}
        handleNewCategoryCreation={handleNewCategoryCreation}
      />
      {cards.map((el) => {
        if (el.category === selectedCategory) {
          return (
            <Card
              key={el.card_number}
              frontText={el.question}
              rearText={el.answer}
            />
          )
        }
      })}
    </div>
  );
}


//
// class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       cards: [],
//       categories: [],
//       selectedCategory: null,
//     };
//   }
//
// componentDidMount() {
//   axios.get('http://127.0.0.1:5000/cards')
//   .then((response) => {
//     this.setState({
//       cards: response.data,
//     });
//   })
//   .catch((error) => {
//     console.log(error);
//   })
//
//   axios.get('http://127.0.0.1:5000/cards/categories')
//   .then((response) => {
//     this.setState({
//       categories: response.data,
//       selectedCategory: response.data[0].category,
//     });
//   })
//   .catch((error) => {
//     console.log(error);
//   })
// }
//
//   handleSelectCategory = (e) => {
//     this.setState({
//       selectedCategory: e.target.value,
//     });
//   }
//
//   handleAddedCard = () => {
//     axios.get('http://127.0.0.1:5000/cards')
//     .then((response) => {
//       this.setState({
//         cards: response.data,
//       });
//     })
//     .catch((error) => {
//       console.log(error);
//     })
//   }
//
//   handleNewCategoryCreation = (newCategory) => {
//     axios.get('http://127.0.0.1:5000/cards/categories')
//     .then((response) => {
//       this.setState({
//         categories: response.data,
//         selectedCategory: response.data[0].category,
//       });
//     })
//     .catch((error) => {
//       console.log(error);
//     })
//   }
//
//
//
//
//   render() {
//     return (
//       <div className="App">
//         <Header
//           categories={this.state.categories}
//           handleSelectCategory={this.handleSelectCategory}
//           selectedCategory={this.state.selectedCategory}
//           handleAddedCard={this.handleAddedCard}
//           handleNewCategoryCreation={this.handleNewCategoryCreation}
//         />
//         {this.state.cards.map((el) => {
//           if (el.category === this.state.selectedCategory) {
//             return (
//               <Card
//                 key={el.card_number}
//                 frontText={el.question}
//                 rearText={el.answer}
//               />
//             )
//           }
//         })}
//       </div>
//     );
//   }
// }


export default App;
