import './styles/Card.css'
import React, { useState } from 'react';

const Card = ({ frontText, rearText }) => {
  const [flipped, setFlipped] = useState(false);

  const handleCardClick = () => {
    setFlipped(!flipped)
  }

  return (
    <div
      className={`card ${flipped ? 'flipped' : ''}`}
      onClick={handleCardClick}
    >
      <div className="front">
        {frontText}
      </div>
      <div className="rear">
        {rearText}
      </div>
    </div>
  )

}

export default Card;
