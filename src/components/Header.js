import './styles/Header.css'
import React, {useState, useRef, useEffect} from 'react';
import axios from 'axios';

const Header = ({ categories, selectedCategory, handleAddedCard, handleNewCategoryCreation, handleSelectCategory}) => {
  const [question, setQuestion] = useState('Enter Question..');
  const [answer, setAnswer] = useState('Enter Answer..');
  const [addCategory, setAddCategory] = useState('Enter New Category..');
  const selectRef = useRef()

  useEffect(() => {
    console.log('selectedCategory changed')
    selectRef.current.value = selectedCategory;
  }, [selectedCategory])

  const handleChange = (e) => {
    if (e.target.id === 'question') {
      setQuestion(e.target.value);
    }
    if (e.target.id === 'answer') {
      setAnswer(e.target.value);
    }
    if (e.target.id === 'addCategory') {
      setAddCategory(e.target.value);
    }
  }

  const handleAddCardSubmit = (e) => {
    e.preventDefault()

    if (question === 'Enter Question..' ||
        question === '' ||
        answer === 'Enter Answer..' ||
        answer === '') {
          alert('you need to add a question and answer before submitting!');
          return;
        }

    let categoriesID = null;
    for (let each of categories) {
      if (each.category === selectedCategory) {
        categoriesID = each.category_id;
      }
    }

    axios.post('http://127.0.0.1:5000/cards/addCard',
    {
      question: question,
      answer: answer,
      category: categoriesID,
    })
    .then((response) => {
      handleAddedCard();
      setQuestion('Enter Question..');
      setAnswer('Enter Answer..')
    })
    .catch((error) => {
      console.log(error);
    })
  }

  const handleAddCategorySubmit = (e) => {
    e.preventDefault();

    if (addCategory === 'Enter New Category..' ||
        addCategory === '') {
      alert('you need to add a category before submitting')
      return;
    }

    axios.post('http://127.0.0.1:5000/categories/addCategory',
    {
      category: addCategory,
    })
    .then((response) => {
      handleNewCategoryCreation()
    })
    .catch((error) => {
      console.log(error);
    })
    setAddCategory('Enter New Category..')
  }

  return (
    <div className="Header">
      <form className="addCard-form" onSubmit={handleAddCardSubmit}>
        <div className="addCard-title">Add A Card: </div>
        <div>
          <label className="label" htmlFor="question">Q: </label>
          <input
            type="text"
            id="question"
            placeholder={question}
            onChange={handleChange}
            value={question}
            onClick={() =>{setQuestion('')}}
          ></input>
        </div>
        <div>
          <label className="label" htmlFor="answer">A: </label>
          <input type="text"
            id="answer"
            placeholder={answer}
            onChange={handleChange}
            value={answer}
            onClick={() =>{setAnswer('')}}
          ></input>
        </div>
        <div className="submit-btn-container" >
          <input className="submit-btn" type="submit" value="Add Card"></input>
        </div>
      </form>
      <form className="addCategory-form" onSubmit={handleAddCategorySubmit}>
        <div className="addCategory-title">Add A Category:</div>
        <div className="addCategory-container">
          <input type="text"
            id="addCategory"
            placeholder={addCategory}
            onChange={handleChange}
            value={addCategory}
            onClick={() =>{setAddCategory('')}}
          ></input>
        </div>
        <div className="category-submit-btn-container" >
          <input className="category-submit-btn" type="submit" value="Add Category"></input>
        </div>
      </form>
      <form className="category-form">
        <label className="label" htmlFor="category">Category:&nbsp;</label>
        <select ref={selectRef} id="category" onChange={handleSelectCategory} selected={selectedCategory}>
          {categories.map((el,i) => {
            return <option key={i} value={el.category}>{el.category}</option>
          })}
        </select>
      </form>
    </div>
  )

}


export default Header;
